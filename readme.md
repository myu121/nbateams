# NBA Teams App

This is an simple iOS application to display NBA teams, add favorite teams, and show a scoreboard.


## NBA Team List

Here you can tap the heart button to add a team to your favorite team screen.

<img src="https://media.giphy.com/media/Q6FRnqQqGhwzQI3Unx/giphy.gif">

## Scoreboard

Here is a scoreboard filled with dummy data.

<img src="https://i.imgur.com/WmAKfac.png">



## Techniques

- JSON parsing
- UIKit
- Table View
- Model-View-Controller (MVC) code structure
- Delegates

## Technologies

- iOS 13.0 or above

- Xcode 12.5

- Swift 5

## Frameworks

- UIKit

## Supported Devices

- iPhone SE (2nd gen)

- iPhone 8 - 12 (All sizes supported)
