//
//  FavoriteTeamController.swift
//  TableViewPractice
//
//  Created by Michael Yu on 3/23/21.
//

import Foundation
import UIKit

class FavoriteTeamViewController: UITableViewController {
    private var favoriteTeams: [NbaTeam] = []
    private var nbaTeams: [NbaTeam] = []
    private var nbaTeamViewController: NbaTeamViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Favorite NBA Teams"
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.allowsSelection = false
        self.populateDataFromJson()
        
        // Do any additional setup after loading the view.
    }
    
    
    private func populateDataFromJson() {
        if let path = Bundle.main.path(forResource: "nba", ofType: "json") {
            do {
                let dataJson = try Data(contentsOf: URL(fileURLWithPath: path))
                let jsonDict = try JSONSerialization.jsonObject(with: dataJson, options: .mutableContainers)
                if let jsonResults = jsonDict as? [[String: Any]] {
                    jsonResults.forEach { teamDict in
                        nbaTeams.append(NbaTeam(
                                            teamName: teamDict["teamName"] as! String,
                                            teamCity: teamDict["city"] as! String,
                                            teamState: teamDict["state"] as! String,
                                            simpleName: teamDict["simpleName"] as! String)
                        )
                    }
                }
            } catch {
                print("No json data found")
            }
        }
    }

    @IBAction func openNbaTeamView(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let nbaTeamViewController = storyboard.instantiateViewController(withIdentifier: "NbaTeamViewController") as? NbaTeamViewController else {
            return
        }
        self.nbaTeamViewController = nbaTeamViewController
        nbaTeamViewController.nbaTeams = self.nbaTeams
        
        self.navigationController?.pushViewController(nbaTeamViewController, animated: true)
    }
}

extension FavoriteTeamViewController: NbaTeamViewCellDelegate {
    func didFavoriteChange(team: NbaTeam, isFavorite: Bool) {
        let index = nbaTeams.firstIndex { removeTeam in
            removeTeam === team
        } ?? 0
        nbaTeams.removeAll { removeTeam in
            removeTeam === team
        }
        team.isFavorite = isFavorite

        nbaTeams.insert(team, at: index)
        nbaTeams.forEach { team in
            print("\(team.simpleName) \(team.isFavorite)")
        }
        
        favoriteTeams = nbaTeams.filter { $0.isFavorite }
        print(favoriteTeams)
        self.tableView.reloadData()
        
    }
}

extension FavoriteTeamViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        favoriteTeams.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FavoriteTeamViewCell", for: indexPath) as? FavoriteTeamViewCell else {
            fatalError()
        }
        
        
        cell.configure(teamName: favoriteTeams[indexPath.row].teamName, city: favoriteTeams[indexPath.row].teamCity, state: favoriteTeams[indexPath.row].teamState)
        
        let logoIdentifier = favoriteTeams[indexPath.row].simpleName
        if let logo = UIImage(named: logoIdentifier) {
            cell.setTeamLogo(logo)
        }

        return cell
    }
}
