//
//  FavoriteTeamViewCell.swift
//  TableViewPractice
//
//  Created by Michael Yu on 3/22/21.
//

import Foundation
import UIKit

class FavoriteTeamViewCell: UITableViewCell {
    @IBOutlet private weak var teamNameLabel: UILabel!
    @IBOutlet private weak var cityLabel: UILabel!
    @IBOutlet private weak var stateLabel: UILabel!
    @IBOutlet private weak var teamLogo: UIImageView!


    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(teamName: String, city: String, state: String) {
        teamNameLabel.text = teamName
        cityLabel.text = city
        stateLabel.text = state
    }
    
    func setTeamLogo(_ teamLogoImage: UIImage) {
        teamLogo.image = teamLogoImage
    }
    
    
}
