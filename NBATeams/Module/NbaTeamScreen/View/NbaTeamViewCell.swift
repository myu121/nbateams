//
//  TableScreenViewCell.swift
//  TableViewPractice
//
//  Created by Michael Yu on 3/22/21.
//

import Foundation
import UIKit

protocol NbaTeamViewCellDelegate: AnyObject {
    func didFavoriteChange(team: NbaTeam, isFavorite: Bool)
}

class NbaTeamViewCell: UITableViewCell {
    @IBOutlet private weak var teamNameLabel: UILabel!
    @IBOutlet private weak var cityLabel: UILabel!
    @IBOutlet private weak var stateLabel: UILabel!
    @IBOutlet private weak var teamLogo: UIImageView!
    @IBOutlet private weak var heartButton: UIButton!
    weak var delegate: NbaTeamViewCellDelegate?
    
    private var team: NbaTeam?
    
    func configure(team: NbaTeam) {
        self.team = team
        teamNameLabel.text = team.teamName
        cityLabel.text = team.teamCity
        stateLabel.text = team.teamState
        heartButton.isSelected = team.isFavorite
    }
    
    func setTeamLogo(_ teamLogoImage: UIImage) {
        teamLogo.image = teamLogoImage
    }
    
    @IBAction func toggleHeart(sender: UIButton) {
        heartButton.isSelected = !heartButton.isSelected
        guard let team = self.team else { return }
        delegate?.didFavoriteChange(team: team, isFavorite: heartButton.isSelected)
    }
}
