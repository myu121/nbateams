//
//  TableModel.swift
//  TableViewPractice
//
//  Created by Michael Yu on 3/22/21.
//

import Foundation

class NbaTeam {
    var teamName: String
    var teamCity: String
    var teamState: String
    var simpleName: String
    var isFavorite = false
    
    init(teamName: String,
        teamCity: String,
        teamState: String,
        simpleName: String) {
        self.teamCity = teamCity
        self.simpleName = simpleName
        self.teamState = teamState
        self.teamName = teamName
    }
}
