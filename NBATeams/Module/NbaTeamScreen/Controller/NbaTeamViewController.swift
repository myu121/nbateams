//
//  ViewController.swift
//  TableViewPractice
//
//  Created by Michael Yu on 3/22/21.
//
import Foundation
import UIKit

class NbaTeamViewController: UITableViewController {
    var nbaTeams: [NbaTeam] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "NBA Teams"
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.allowsSelection = false
    }
}

extension NbaTeamViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        nbaTeams.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NbaTeamViewCell", for: indexPath) as? NbaTeamViewCell else {
            fatalError()
        }

        cell.configure(team: nbaTeams[indexPath.row])
        
        let logoIdentifier = nbaTeams[indexPath.row].simpleName
        if let logo = UIImage(named: logoIdentifier) {
            cell.setTeamLogo(logo)
        }
        
        
        cell.delegate = self.navigationController?.viewControllers.first { $0 is FavoriteTeamViewController } as? FavoriteTeamViewController
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            nbaTeams.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
}

