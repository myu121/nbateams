//
//  WCGameTableViewCell.swift
//  TableViewPractice
//
//  Created by Michael Yu on 3/30/21.
//

import UIKit

class WCScoreTableViewCell: UITableViewCell {
    @IBOutlet private weak var country1Label: UILabel!
    @IBOutlet private weak var country2Label: UILabel!
    @IBOutlet private weak var country1ScoreLabel: UILabel!
    @IBOutlet private weak var country2ScoreLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var minuteLabel: UILabel!


    func configure(country1: String, country2: String, country1Score: String, country2Score: String, title: String, minute: String) {
        country1Label.text = country1
        country2Label.text = country2
        country1ScoreLabel.text = country1Score
        country2ScoreLabel.text = country2Score
        titleLabel.text = title
        minuteLabel.text = minute
    }
}
