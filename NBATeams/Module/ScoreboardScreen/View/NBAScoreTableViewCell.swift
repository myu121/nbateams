//
//  NBAScoreTableViewCell.swift
//  TableViewPractice
//
//  Created by Michael Yu on 3/30/21.
//

import UIKit

class NBAScoreTableViewCell: UITableViewCell {
    @IBOutlet private weak var team1Label: UILabel!
    @IBOutlet private weak var team2Label: UILabel!
    @IBOutlet private weak var team1RecordLabel: UILabel!
    @IBOutlet private weak var team2RecordLabel: UILabel!
    @IBOutlet private weak var startTimeLabel: UILabel!
    @IBOutlet private weak var bettingLineLabel: UILabel!
    @IBOutlet private weak var team1ImageView: UIImageView!
    @IBOutlet private weak var team2ImageView: UIImageView!

    func configure(team1: String, team2: String, team1Record: String, team2Record: String, startTime: String, bettingLine: String) {
        team1Label.text = team1
        team2Label.text = team2
        team1RecordLabel.text = team1Record
        team2RecordLabel.text = team2Record
        startTimeLabel.text = startTime
        bettingLineLabel.text = bettingLine
        team1ImageView.image = UIImage(named: team1Label.text ?? "")
        team2ImageView.image = UIImage(named: team2Label.text ?? "")
    }
}
