//
//  NCAAScoreTableViewCell.swift
//  TableViewPractice
//
//  Created by Michael Yu on 3/30/21.
//

import UIKit

class NCAAScoreTableViewCell: UITableViewCell {
    @IBOutlet private weak var team1Label: UILabel!
    @IBOutlet private weak var team2Label: UILabel!
    @IBOutlet private weak var team1RecordLabel: UILabel!
    @IBOutlet private weak var team2RecordLabel: UILabel!
    @IBOutlet private weak var team1SeedLabel: UILabel!
    @IBOutlet private weak var team2SeedLabel: UILabel!
    @IBOutlet private weak var startTimeLabel: UILabel!
    @IBOutlet private weak var bettingLineLabel: UILabel!
    @IBOutlet private weak var subtitleLabel: UILabel!

    func configure(team1: String, team2: String, team1Record: String, team2Record: String, team1Seed: String, team2Seed: String, startTime: String, bettingLine: String, subtitle: String) {
        team1Label.text = team1
        team2Label.text = team2
        team1RecordLabel.text = team1Record
        team2RecordLabel.text = team2Record
        team1SeedLabel.text = team1Seed
        team2SeedLabel.text = team2Seed
        startTimeLabel.text = startTime
        bettingLineLabel.text = bettingLine
        subtitleLabel.text = subtitle
    }
}
