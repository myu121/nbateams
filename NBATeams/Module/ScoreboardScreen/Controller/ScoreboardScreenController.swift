//
//  NBAScoreboardScreenController.swift
//  TableViewPractice
//
//  Created by Michael Yu on 3/30/21.
//

import Foundation
import UIKit

class ScoreboardScreenController: UIViewController {
    @IBOutlet private weak var tableView: UITableView! {
        didSet{
            self.tableView.delegate = self
            self.tableView.dataSource = self
            //self.tableView.tableFooterView = UIView()
            self.tableView.allowsSelection = false
            self.tableView.sectionFooterHeight = 0
        }
    }
    private var sectionDict: [String: [Any]] = [:]
    var sections = [Section]()
    
    private func populateDataFromJson() {
        if let path = Bundle.main.path(forResource: "score", ofType: "json") {
            do {
                let dataJson = try Data(contentsOf: URL(fileURLWithPath: path))
                let jsonDict = try JSONSerialization.jsonObject(with: dataJson, options: .mutableContainers)
                if let jsonResults = jsonDict as? [[String: String]] {
                    jsonResults.forEach { gameDict in
                        let league = gameDict["league"]
                        switch league {
                        case "NBA":
                            if sectionDict["NBA"] != nil {
                                sectionDict["NBA"]?.append(NBAGame(dict: gameDict))
                            } else {
                                sectionDict["NBA"] = [NBAGame(dict: gameDict)]
                            }
                        case "NCAAM":
                            if sectionDict["NCAAM"] != nil {
                                sectionDict["NCAAM"]?.append(NCAAGame(dict: gameDict))
                            } else {
                                sectionDict["NCAAM"] = [NCAAGame(dict: gameDict)]
                            }
                        case "WC QUAL - UEFA":
                            if sectionDict["WC QUAL - UEFA"] != nil {
                                sectionDict["WC QUAL - UEFA"]?.append(WCGame(dict: gameDict))
                            } else {
                                sectionDict["WC QUAL - UEFA"] = [WCGame(dict: gameDict)]
                            }
                        case .none:
                            fallthrough
                        case .some(_):
                            break
                        }
                    }
                }
            } catch {
                print("No json data found")
            }
        }
    }
    
    private func prepareSectionsArray() {
        let keys = sectionDict.keys.sorted()
        sections = keys.map {
            var section = Section()
            if let games = sectionDict[$0] {
                section = Section(league: $0, games: games)
            }
            return section
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateDataFromJson()
        prepareSectionsArray()
    }
}

extension ScoreboardScreenController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        35.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.init(cgColor: CGColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1))
        let myLabel = UILabel()
        myLabel.frame = CGRect(x: 20, y: 8, width: 320, height: 20)
        myLabel.font = UIFont.boldSystemFont(ofSize: 18)
        myLabel.text = self.tableView(tableView, titleForHeaderInSection: section)
        headerView.addSubview(myLabel)
        return headerView
    }
    
}

extension ScoreboardScreenController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        sections[section].games?.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        sections.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        sections[section].league
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = sections[indexPath.section]
        switch section.league {
            case "NBA":
                let game = section.games?[indexPath.row] as? NBAGame
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "NBAScoreTableViewCell", for: indexPath) as? NBAScoreTableViewCell else {
                    fatalError("Cell not found")
                }
                cell.configure(team1: game?.homeTeam ?? "",
                               team2: game?.awayTeam ?? "",
                               team1Record: game?.homeTeamRecord ?? "",
                               team2Record: game?.awayTeamRecord ?? "",
                               startTime: game?.startTime ?? "",
                               bettingLine: game?.bettingLine ?? "")
                return cell
            case "NCAAM":
                let game = section.games?[indexPath.row] as? NCAAGame
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "NCAAScoreTableViewCell", for: indexPath) as? NCAAScoreTableViewCell else {
                    fatalError("Cell not found")
                }
                cell.configure(team1: game?.team1 ?? "",
                               team2: game?.team2 ?? "",
                               team1Record: game?.team1Record ?? "",
                               team2Record: game?.team2Record ?? "",
                               team1Seed: game?.team1Seed ?? "",
                               team2Seed: game?.team2Seed ?? "",
                               startTime: game?.startTime ?? "",
                               bettingLine: game?.bettingLine ?? "",
                               subtitle: game?.subtitle ?? "")
                return cell
            case "WC QUAL - UEFA":
                let game = section.games?[indexPath.row] as? WCGame
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "WCScoreTableViewCell", for: indexPath) as? WCScoreTableViewCell else {
                    fatalError("Cell not found")
                }
                cell.configure(country1: game?.country1 ?? "",
                               country2: game?.country2 ?? "",
                               country1Score: game?.country1Score ?? "",
                               country2Score: game?.country2Score ?? "",
                               title: game?.title ?? "",
                               minute: game?.minute ?? "")
                return cell
        case .none:
            break
        case .some(_):
            break
        }
        return UITableViewCell()
    }
}
