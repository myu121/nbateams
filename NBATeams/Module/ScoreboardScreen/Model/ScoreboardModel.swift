//
//  NBAScoreboardModel.swift
//  TableViewPractice
//
//  Created by Michael Yu on 3/30/21.
//

import Foundation

struct NBAGame {
    var league: String?
    var homeTeam: String?
    var awayTeam: String?
    var homeTeamRecord: String?
    var awayTeamRecord: String?
    var startTime: String?
    var bettingLine: String?
    
    init(dict: [String: String]) {
        league = dict["league"]
        homeTeam = dict["homeTeam"]
        awayTeam = dict["awayTeam"]
        homeTeamRecord = dict["homeTeamRecord"]
        awayTeamRecord = dict["awayTeamRecord"]
        startTime = dict["startTime"]
        bettingLine = dict["bettingLine"]
    }
}

struct NCAAGame {
    var league: String?
    var team1: String?
    var team2: String?
    var team1Record: String?
    var team2Record: String?
    var team1Seed: String?
    var team2Seed: String?
    var startTime: String?
    var subtitle: String?
    var bettingLine: String?
    
    init(dict: [String: String]) {
        league = dict["league"]
        team1 = dict["team1"]
        team2 = dict["team2"]
        team1Record = dict["team1Record"]
        team2Record = dict["team2Record"]
        startTime = dict["startTime"]
        bettingLine = dict["bettingLine"]
        team1Seed = dict["team1Seed"]
        team2Seed = dict["team2Seed"]
        subtitle = dict["subtitle"]
    }
}

struct WCGame {
    var league: String?
    var title: String?
    var country1: String?
    var country2: String?
    var country1Score: String?
    var country2Score: String?
    var minute: String?
    
    init(dict: [String: String]) {
        league = dict["league"]
        country1 = dict["country1"]
        country2 = dict["country2"]
        country1Score = dict["country1Score"]
        country2Score = dict["country2Score"]
        minute = dict["minute"]
        title = dict["title"]
    }
}

struct Section {
    var league: String?
    var games: [Any]?
}
